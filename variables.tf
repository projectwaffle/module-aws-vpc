## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource Section variables

# VPC Section Variables

variable "create_vpc" {
  description = "Boolean Variable that allows module to Ignore VPC creation"
  default     = "true"
}

variable "cidr_block" {
  description = "The CIDR block for the VPC. Default value is valid one, however to be changed"
  default     = "100.79.0.0/19"
}

variable "assign_generated_ipv6_cidr_block" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block"
  default     = false
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = false
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  default     = true
}

variable "vpc_tags" {
  description = "Additional tags for the VPC"
  default     = {}
}

# DHCP Options

variable "enable_dhcp_options" {
  description = "Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type"
  default     = false
}

variable "dhcp_options_domain_name" {
  description = "Specifies DNS name for DHCP options set"
  default     = ""
}

variable "dhcp_options_domain_name_servers" {
  description = "Specify a list of DNS server addresses for DHCP options set, default to AWS provided"
  type        = "list"
  default     = ["AmazonProvidedDNS"]
}

variable "dhcp_options_ntp_servers" {
  description = "Specify a list of NTP servers for DHCP options set"
  type        = "list"
  default     = []
}

variable "dhcp_options_netbios_name_servers" {
  description = "Specify a list of netbios servers for DHCP options set"
  type        = "list"
  default     = []
}

variable "dhcp_options_netbios_node_type" {
  description = "Specify netbios node_type for DHCP options set"
  default     = ""
}

variable "dhcp_options_tags" {
  description = "Additional tags for the DHCP Options Set"
  default     = {}
}

# Internet Gateway Variables

variable "internet_gateway_tags" {
  description = "Additional tags for the VPC Internet Gateway"
  default     = {}
}

# NAT gateway Configuration

variable "enable_nat_gateway" {
  description = "Should be true if you want Private Subnets route default via NAT Gateway"
  default     = "false"
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`."
  default     = "fase"
}

variable "eip_nat_tags" {
  description = "Additional tags for the NAT Gateway Elastic IP addresses"
  default     = {}
}

variable "nat_gateway_tags" {
  description = "Additional tags for the NAT Gateways"
  default     = {}
}

# Public Routing

variable "public_subnets" {
  description = "Pulic Subnet IP Addressing Schema"
  default     = []
}

variable "map_public_ip_on_launch" {
  description = "Should be false if you do not want to auto-assign public IP on launch"
  default     = true
}

variable "public_subnet_tags" {
  description = "Additional tags for the public subnets"

  default = {
    role = "public"
  }
}

variable "route_table_public_tags" {
  description = "Additional tags for the Public Routing Table"
  default     = {}
}

# DMZ Routing

variable "create_dmz" {
  description = "Set to true if DMZ networks are needed, if enabled, set dmz_subnets variable in module Configuration"
  default     = false
}

variable "dmz_subnets" {
  description = "Private Subnet IP Addressing Schema"
  default     = []
}

variable "dmz_subnet_tags" {
  description = "Additional tags for the private subnets"

  default = {
    role = "dmz"
  }
}

variable "route_table_dmz_tags" {
  description = "Additional tags for the Private Routing Table"
  default     = {}
}

# Private Routing

variable "private_subnets" {
  description = "Private Subnet IP Addressing Schema"
  default     = []
}

variable "private_subnet_tags" {
  description = "Additional tags for the private subnets"

  default = {
    role = "private"
  }
}

variable "route_table_private_tags" {
  description = "Additional tags for the Private Routing Table"
  default     = {}
}

# Intra Routing

variable "create_intra" {
  description = "Set to true if Intra networks are needed, if enabled, set intra_subnets variable in module Configuration"
  default     = false
}

variable "intra_subnets" {
  description = "Intra Subnet IP Addressing Schema"
  default     = []
}

variable "intra_subnet_tags" {
  description = "Additional tags for the intra subnets"

  default = {
    role = "intra"
  }
}

variable "route_table_intra_tags" {
  description = "Additional tags for the Intra Routing Table"
  default     = {}
}
