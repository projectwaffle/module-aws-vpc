output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${element(concat(aws_vpc.main.*.id, list("")), 0)}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = "${element(concat(aws_vpc.main.*.cidr_block, list("")), 0)}"
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = ["${aws_subnet.private.*.id}"]
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value       = ["${aws_subnet.private.*.cidr_block}"]
}

output "private_subnets_azs" {
  description = "List of azs of private subnets"
  value       = ["${aws_subnet.private.*.availability_zone}"]
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = ["${aws_subnet.public.*.id}"]
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = ["${aws_subnet.public.*.cidr_block}"]
}

output "public_subnets_azs" {
  description = "List of azs of public subnets"
  value       = ["${aws_subnet.public.*.availability_zone}"]
}

output "intra_subnets" {
  description = "List of IDs of intra subnets"
  value       = ["${aws_subnet.intra.*.id}"]
}

output "intra_subnets_cidr_blocks" {
  description = "List of cidr_blocks of intra subnets"
  value       = ["${aws_subnet.intra.*.cidr_block}"]
}

output "intra_subnets_azs" {
  description = "List of azs of intra subnets"
  value       = ["${aws_subnet.intra.*.availability_zone}"]
}

output "dmz_subnets" {
  description = "List of IDs of dmz subnets"
  value       = ["${aws_subnet.dmz.*.id}"]
}

output "dmz_subnets_cidr_blocks" {
  description = "List of cidr_blocks of dmz subnets"
  value       = ["${aws_subnet.dmz.*.cidr_block}"]
}

output "dmz_subnets_azs" {
  description = "List of azs of dmz subnets"
  value       = ["${aws_subnet.dmz.*.availability_zone}"]
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value       = ["${aws_route_table.public.*.id}"]
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value       = ["${aws_route_table.private.*.id}"]
}

output "intra_route_table_ids" {
  description = "List of IDs of intra route tables"
  value       = ["${aws_route_table.intra.*.id}"]
}

output "nat_ids" {
  description = "List of allocation ID of Elastic IPs created for AWS NAT Gateway"
  value       = ["${aws_eip.nat.*.id}"]
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = ["${aws_eip.nat.*.public_ip}"]
}

output "natgw_ids" {
  description = "List of NAT Gateway IDs"
  value       = ["${aws_nat_gateway.main.*.id}"]
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value       = "${element(concat(aws_internet_gateway.main.*.id, list("")), 0)}"
}
