# Locals values generation

locals {
  nat_gateway_count = "${var.enable_nat_gateway ? (var.one_nat_gateway_per_az ? length(var.azs) : 1) : 0}"
}

# VPC 

resource "aws_vpc" "main" {
  count = "${var.create_vpc ? 1 : 0}"

  cidr_block                       = "${var.cidr_block}"
  instance_tenancy                 = "${var.instance_tenancy}"
  enable_dns_hostnames             = "${var.enable_dns_hostnames}"
  enable_dns_support               = "${var.enable_dns_support}"
  assign_generated_ipv6_cidr_block = "${var.assign_generated_ipv6_cidr_block}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["vpc"],)), var.global_tags, var.module_tags, var.vpc_tags)}"
}

# VPC DHCP Options

resource "aws_vpc_dhcp_options" "main" {
  count = "${var.create_vpc && var.enable_dhcp_options ? 1 : 0}"

  domain_name          = "${var.dhcp_options_domain_name}"
  domain_name_servers  = ["${var.dhcp_options_domain_name_servers}"]
  ntp_servers          = ["${var.dhcp_options_ntp_servers}"]
  netbios_name_servers = ["${var.dhcp_options_netbios_name_servers}"]
  netbios_node_type    = "${var.dhcp_options_netbios_node_type}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["dhcp_options"],)), var.global_tags, var.module_tags, var.dhcp_options_tags)}"
}

resource "aws_vpc_dhcp_options_association" "main" {
  count = "${var.create_vpc && var.enable_dhcp_options ? 1 : 0}"

  vpc_id          = "${aws_vpc.main.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.main.id}"
}

# Internet gateway 

resource "aws_internet_gateway" "main" {
  count = "${var.create_vpc && length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["internet_gateway"],)), var.global_tags, var.module_tags, var.internet_gateway_tags)}"
}

##  NAT Gateway

resource "aws_eip" "nat" {
  count = "${var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  vpc = true

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["eip_nat"],)), var.global_tags, var.module_tags, var.eip_nat_tags)}"
}

resource "aws_nat_gateway" "main" {
  count = "${var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["nat_gateway"],)), var.global_tags, var.module_tags, var.nat_gateway_tags)}"

  depends_on = ["aws_internet_gateway.main"]
}

## Routing

# Public Routing

resource "aws_route_table" "public" {
  count = "${var.create_vpc && length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["route_table_public"],)), var.global_tags, var.module_tags, var.route_table_public_tags)}"
}

resource "aws_route" "public_internet_gateway" {
  count = "${var.create_vpc && length(var.public_subnets) > 0 ? 1 : 0}"

  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

# DMZ Routing

resource "aws_route_table" "dmz" {
  count = "${var.create_vpc && var.create_dmz && length(var.azs) > 0 ? (local.nat_gateway_count > 0 ? local.nat_gateway_count : 1) : 0}"

  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["route_table_dmz"],)), var.global_tags, var.module_tags, var.route_table_dmz_tags)}"
}

resource "aws_route" "dmz_internet_gateway" {
  count = "${var.create_vpc && var.create_dmz && !var.enable_nat_gateway && length(var.azs) > 0 ? 1 : 0}"

  route_table_id         = "${aws_route_table.dmz.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route" "dmz_nat_gateway" {
  count = "${var.create_vpc && var.create_dmz && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  route_table_id         = "${element(aws_route_table.dmz.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.main.*.id, count.index)}"
}

# Private Routing

resource "aws_route_table" "private" {
  count = "${var.create_vpc && length(var.azs) > 0 ? (local.nat_gateway_count > 0 ? local.nat_gateway_count : 1) : 0}"

  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["route_table_private"],)), var.global_tags, var.module_tags, var.route_table_private_tags)}"
}

resource "aws_route" "private_internet_gateway" {
  count = "${var.create_vpc && !var.enable_nat_gateway && length(var.azs) > 0 ? 1 : 0}"

  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route" "private_nat_gateway" {
  count = "${var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.main.*.id, count.index)}"
}

# Intra Routing

resource "aws_route_table" "intra" {
  count = "${var.create_vpc && var.create_intra && length(var.azs) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["route_table_intra"],)), var.global_tags, var.module_tags, var.route_table_intra_tags)}"
}

## Subnets

# Public Subnets

resource "aws_subnet" "public" {
  count = "${var.create_vpc && length(var.public_subnets) > 0 && (!var.one_nat_gateway_per_az || length(var.public_subnets) >= length(var.azs)) ? length(var.public_subnets) : 0}"

  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${element(concat(var.public_subnets, list("")), count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = "${merge(map("Name", format("%s-%s-public-%s", var.name, var.resource_identities["subnet"], element(var.azs, count.index))), var.global_tags, var.module_tags, var.public_subnet_tags)}"
}

# Private Subnets

resource "aws_subnet" "private" {
  count = "${var.create_vpc && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0}"

  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.private_subnets[count.index]}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "${!var.enable_nat_gateway ? var.map_public_ip_on_launch : false}"

  tags = "${merge(map("Name", format("%s-%s-private-%s", var.name, var.resource_identities["subnet"], element(var.azs, count.index))), var.global_tags, var.module_tags, var.private_subnet_tags)}"
}

# DMZ Subnets

resource "aws_subnet" "dmz" {
  count = "${var.create_vpc && var.create_dmz && length(var.dmz_subnets) > 0 ? length(var.dmz_subnets) : 0}"

  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.dmz_subnets[count.index]}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = "${merge(map("Name", format("%s-%s-dmz-%s", var.name, var.resource_identities["subnet"], element(var.azs, count.index))), var.global_tags, var.module_tags, var.dmz_subnet_tags)}"
}

# Intra Subnets

resource "aws_subnet" "intra" {
  count = "${var.create_vpc && var.create_intra && length(var.intra_subnets) > 0 ? length(var.intra_subnets) : 0}"

  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "${var.intra_subnets[count.index]}"
  availability_zone = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-%s-intra-%s", var.name, var.resource_identities["subnet"], element(var.azs, count.index))), var.global_tags, var.module_tags, var.intra_subnet_tags)}"
}

## Route Association

# Public Association

resource "aws_route_table_association" "public" {
  count = "${var.create_vpc && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

# Private Association

resource "aws_route_table_association" "private_internet_gateway" {
  count = "${var.create_vpc && !var.enable_nat_gateway && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private_nat_gateway" {
  count = "${var.create_vpc && var.enable_nat_gateway && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${local.nat_gateway_count > 1 ? element(aws_route_table.private.*.id, count.index) : element(aws_route_table.private.*.id, 0)}"
}

# DMZ Association

resource "aws_route_table_association" "dmz_internet_gateway" {
  count = "${var.create_vpc && var.create_dmz && !var.enable_nat_gateway && length(var.dmz_subnets) > 0 ? length(var.dmz_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.dmz.*.id, count.index)}"
  route_table_id = "${aws_route_table.dmz.id}"
}

resource "aws_route_table_association" "dmz_nat_gateway" {
  count = "${var.create_vpc && var.create_dmz && var.enable_nat_gateway && length(var.dmz_subnets) > 0 ? length(var.dmz_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.dmz.*.id, count.index)}"
  route_table_id = "${local.nat_gateway_count > 1 ? element(aws_route_table.dmz.*.id, count.index) : element(aws_route_table.dmz.*.id, 0)}"
}

# Intra Association

resource "aws_route_table_association" "intra" {
  count = "${var.create_vpc && var.create_intra && length(var.azs) > 0 ? length(var.intra_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.intra.*.id, count.index)}"
  route_table_id = "${aws_route_table.intra.id}"
}
