# AWS VPC Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-vpc.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-vpc/)
[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg?style=svg)](LICENSE)


Terraform module which creates VPC resources on AWS.

These types of resources are supported:

* [VPC](https://www.terraform.io/docs/providers/aws/r/vpc.html)
* [Subnet](https://www.terraform.io/docs/providers/aws/r/subnet.html)
* [Route](https://www.terraform.io/docs/providers/aws/r/route.html)
* [Route table](https://www.terraform.io/docs/providers/aws/r/route_table.html)
* [Internet Gateway](https://www.terraform.io/docs/providers/aws/r/internet_gateway.html)
* [NAT Gateway](https://www.terraform.io/docs/providers/aws/r/nat_gateway.html)
* [VPN Gateway](https://www.terraform.io/docs/providers/aws/r/vpn_gateway.html)
* [VPC Endpoint](https://www.terraform.io/docs/providers/aws/r/vpc_endpoint.html) (S3 and DynamoDB)
* [RDS DB Subnet Group](https://www.terraform.io/docs/providers/aws/r/db_subnet_group.html)
* [ElastiCache Subnet Group](https://www.terraform.io/docs/providers/aws/r/elasticache_subnet_group.html)
* [Redshift Subnet Group](https://www.terraform.io/docs/providers/aws/r/redshift_subnet_group.html)
* [DHCP Options Set](https://www.terraform.io/docs/providers/aws/r/vpc_dhcp_options.html)
* [Default VPC](https://www.terraform.io/docs/providers/aws/r/default_vpc.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)


## Usage

```hcl
module "vpc" {
  source  = "bitbucket.org/projectwaffle/module-aws-vpc.git?ref=tags/v0.0.1"

  resource_identities = "${var.resource_identities}"
  global_tags         = "${var.global_tags}"

  cidr_block             = "10.49.0.0/16"
  enable_nat_gateway     = "false"
  one_nat_gateway_per_az = "false"

  azs            = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  public_subnets = ["10.49.0.0/23", "10.49.2.0/23", "10.49.4.0/23"]

  create_dmz      = "true"
  dmz_subnets     = ["10.49.10.0/23", "10.49.12.0/23", "10.49.14.0/23"]
  private_subnets = ["10.49.20.0/23", "10.49.22.0/23", "10.49.24.0/23"]

  create_intra  = "true"
  intra_subnets = ["10.49.30.0/23", "10.49.32.0/23", "10.49.34.0/23"]
}
```

## NAT Gateway Scenarios

This module supports three scenarios for creating NAT gateways. Each will be explained in further detail in the corresponding sections.

* No NAT Gateway, internet over Internet Gateway
    * `enable_nat_gateway = false`
    * `one_nat_gateway_per_az = false`
* Single NAT Gateway
    * `enable_nat_gateway = true`
    * `one_nat_gateway_per_az = false`
* One NAT Gateway per availability zone
    * `enable_nat_gateway = true`
    * `one_nat_gateway_per_az = true`

If both cases `enable_nat_gateway` and `one_nat_gateway_per_az` are set to `true`, then `enable_nat_gateway` takes precedence.
Module creates for each VPN Gateway dedicated Elastic IP

### No NAT Gateway (default) internet avaiable via Internet Gateway

By default, module will not create any NAT gateway. All subnets will have internet access over Internet gateway.

### Single NAT Gateway

If `enable_nat_gateway = true`, then all private subnets will route their Internet traffic through this single NAT gateway. The NAT gateway will be placed in the first public subnet in your `public_subnets` block.

### One NAT Gateway per availability zone

If `one_nat_gateway_per_az = true` and `enable_nat_gateway = false`, then the module will place one NAT gateway in each availability zone you specify in `var.azs`. There are some requirements around using this feature flag:

* The variable `var.azs` **must** be specified.
* The number of public subnet CIDR blocks specified in `public_subnets` **must** be greater than or equal to the number of availability zones specified in `var.azs`. This is to ensure that each NAT Gateway has a dedicated public subnet to deploy to.

## "private" versus "intra" subnets

By default, if NAT Gateways are enabled, private subnets will be configured with routes for Internet traffic that point at the NAT Gateways configured by use of the above options.

If you need private subnets that should have no Internet routing (in the sense of [RFC1918 Category 1 subnets](https://tools.ietf.org/html/rfc1918)), `intra_subnets` should be specified. An example use case is configuration of AWS Lambda functions within a VPC, where AWS Lambda functions only need to pass traffic to internal resources or VPC endpoints for AWS services.

Since AWS Lambda functions allocate Elastic Network Interfaces in proportion to the traffic received ([read more](https://docs.aws.amazon.com/lambda/latest/dg/vpc.html)), it can be useful to allocate a large private subnet for such allocations, while keeping the traffic they generate entirely internal to the VPC.

You can add additional tags with `intra_subnet_tags` as with other subnet types.

## Conditional creation

Sometimes you need to have a way to create VPC resources conditionally but Terraform does not allow to use `count` inside `module` block, so the solution is to specify argument `create_vpc`.

```hcl
# This VPC will not be created
module "vpc" {
  source = "./vpc"

  create_vpc = false
  # ... omitted
}
```

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| assign\_generated\_ipv6\_cidr\_block | Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block | string | `false` | no |
| azs | A list of availability zones in the region | list | `<list>` | no |
| cidr\_block | The CIDR block for the VPC. Default value is valid one, however to be changed | string | `100.79.0.0/19` | no |
| create\_dmz | Set to true if DMZ networks are needed, if enabled, set dmz_subnets variable in module Configuration | string | `false` | no |
| create\_intra | Set to true if Intra networks are needed, if enabled, set intra_subnets variable in module Configuration | string | `false` | no |
| create\_vpc | Boolean Variable that allows module to Ignore VPC creation | string | `true` | no |
| dhcp\_options\_domain\_name | Specifies DNS name for DHCP options set | string | `` | no |
| dhcp\_options\_domain\_name\_servers | Specify a list of DNS server addresses for DHCP options set, default to AWS provided | list | `<list>` | no |
| dhcp\_options\_netbios\_name\_servers | Specify a list of netbios servers for DHCP options set | list | `<list>` | no |
| dhcp\_options\_netbios\_node\_type | Specify netbios node_type for DHCP options set | string | `` | no |
| dhcp\_options\_ntp\_servers | Specify a list of NTP servers for DHCP options set | list | `<list>` | no |
| dhcp\_options\_tags | Additional tags for the DHCP Options Set | map | `<map>` | no |
| dmz\_subnet\_tags | Additional tags for the private subnets | map | `<map>` | no |
| dmz\_subnets | Private Subnet IP Addressing Schema | list | `<list>` | no |
| eip\_nat\_tags | Additional tags for the NAT Gateway Elastic IP addresses | map | `<map>` | no |
| enable\_dhcp\_options | Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type | string | `false` | no |
| enable\_dns\_hostnames | Should be true to enable DNS hostnames in the VPC | string | `false` | no |
| enable\_dns\_support | Should be true to enable DNS support in the VPC | string | `true` | no |
| enable\_nat\_gateway | Should be true if you want Private Subnets route default via NAT Gateway | string | `false` | no |
| global\_tags | Global Tags | map | - | yes |
| instance\_tenancy | A tenancy option for instances launched into the VPC | string | `default` | no |
| internet\_gateway\_tags | Additional tags for the VPC Internet Gateway | map | `<map>` | no |
| intra\_subnet\_tags | Additional tags for the intra subnets | map | `<map>` | no |
| intra\_subnets | Intra Subnet IP Addressing Schema | list | `<list>` | no |
| map\_public\_ip\_on\_launch | Should be false if you do not want to auto-assign public IP on launch | string | `true` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| nat\_gateway\_tags | Additional tags for the NAT Gateways | map | `<map>` | no |
| one\_nat\_gateway\_per\_az | Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`. | string | `fase` | no |
| private\_subnet\_tags | Additional tags for the private subnets | map | `<map>` | no |
| private\_subnets | Private Subnet IP Addressing Schema | list | `<list>` | no |
| public\_subnet\_tags | Additional tags for the public subnets | map | `<map>` | no |
| public\_subnets | Pulic Subnet IP Addressing Schema | list | `<list>` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| route\_table\_dmz\_tags | Additional tags for the Private Routing Table | map | `<map>` | no |
| route\_table\_intra\_tags | Additional tags for the Intra Routing Table | map | `<map>` | no |
| route\_table\_private\_tags | Additional tags for the Private Routing Table | map | `<map>` | no |
| route\_table\_public\_tags | Additional tags for the Public Routing Table | map | `<map>` | no |
| vpc\_tags | Additional tags for the VPC | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| dmz\_subnets | List of IDs of dmz subnets |
| dmz\_subnets\_azs | List of azs of dmz subnets |
| dmz\_subnets\_cidr\_blocks | List of cidr_blocks of dmz subnets |
| igw\_id | The ID of the Internet Gateway |
| intra\_route\_table\_ids | List of IDs of intra route tables |
| intra\_subnets | List of IDs of intra subnets |
| intra\_subnets\_azs | List of azs of intra subnets |
| intra\_subnets\_cidr\_blocks | List of cidr_blocks of intra subnets |
| nat\_ids | List of allocation ID of Elastic IPs created for AWS NAT Gateway |
| nat\_public\_ips | List of public Elastic IPs created for AWS NAT Gateway |
| natgw\_ids | List of NAT Gateway IDs |
| private\_route\_table\_ids | List of IDs of private route tables |
| private\_subnets | List of IDs of private subnets |
| private\_subnets\_azs | List of azs of private subnets |
| private\_subnets\_cidr\_blocks | List of cidr_blocks of private subnets |
| public\_route\_table\_ids | List of IDs of public route tables |
| public\_subnets | List of IDs of public subnets |
| public\_subnets\_azs | List of azs of public subnets |
| public\_subnets\_cidr\_blocks | List of cidr_blocks of public subnets |
| vpc\_cidr\_block | The CIDR block of the VPC |
| vpc\_id | The ID of the VPC |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/) 

## License

Licensed under MIT OpenSource Licence Initiate, by Sergiu Plotnicu. Please contact him at - sergiu.plotnicu@gmail.com

